package fr.nantes.iut.tptan.presentation;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import fr.nantes.iut.tptan.R;
import fr.nantes.iut.tptan.data.entity.Arret;
import fr.nantes.iut.tptan.data.entity.ListArret;

public class ProximityStopMapFragment extends Fragment implements OnMapReadyCallback, HomeActivity.ProximityStopListener {

    private GoogleMap mMap;

    public ProximityStopMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_proximity_stop_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = getMapFragment();
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            mapFragment.setRetainInstance(true);
            getChildFragmentManager().beginTransaction().replace(R.id.proximity_stop_map_layout, mapFragment, null).commit();
        }
        mapFragment.getMapAsync(this);
    }

    private SupportMapFragment getMapFragment() {
        return (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.proximity_stop_map_layout);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).registerProximityListener(this);
    }

    @Override
    public void onDetach() {
        ((HomeActivity)getActivity()).unregisterProximityStopListener(this);
        super.onDetach();
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onProximityStopChanged(ListArret arrets, Location location) {
        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
            location.getLongitude())));
        for (Arret arret : arrets) {
            if (arret.getStop() != null && arret.getStop().getLat() != null &&
                arret.getStop().getLng() != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(arret.getStop().getLng(), arret.getStop()
                    .getLat()));
                markerOptions.title(arret.getLibelle());
                markerOptions.draggable(false);
                markerOptions.snippet(arret.getLignes().toString());
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.busmarker));
                mMap.addMarker(markerOptions);
            } else {
                Log.d("OWN", arret.toString());
                Log.d("OWN", arret.getStop().toString());
                Log.d("OWN", String.valueOf(arret.getStop().getLng()));
                Log.d("OWN", String.valueOf(arret.getStop().getLat()));
            }
        }
    }
}
